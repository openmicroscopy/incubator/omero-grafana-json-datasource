# http://flask.pocoo.org/docs/1.0/testing/

import pytest
from base64 import b64encode

from app import app
from mock import (
    patch,
    Mock,
)

from omero.gateway import BlitzGateway
from omero.rtypes import (
    rint,
    rstring,
)


@pytest.fixture
def client():
    app.config['TESTING'] = True
    client = app.test_client()
    yield client


def with_auth(client, method, *args, **kwargs):
    headers = kwargs.get('headers', {})
    headers['Authorization'] = 'Basic ' + b64encode('user:password')
    kwargs['headers'] = headers
    return getattr(client, method.lower())(*args, **kwargs)


def test_home(client):
    rv = client.get('/')
    assert rv.status_code == 200


def test_connect_no_auth(client):
    rv = client.get('/omero.example:4064')
    assert rv.status_code == 401
    assert 'Login required' in rv.data


@patch('app.connect_omero')
def test_connect_server(mock_connect_omero, client):
    m = Mock(autospec=BlitzGateway)
    mock_connect_omero.return_value = m
    rv = with_auth(client, 'GET', '/omero.example:4064')
    assert rv.status_code == 200
    assert m.close.call_count == 1


def test_search(client):
    rv = with_auth(client, 'GET', '/omero.example:4064/search')
    assert rv.status_code == 200
    d = rv.get_json()
    assert sorted(d) == d
    assert 'object_fileset_delete_hour' in d
    assert 'object_project_insert_day' in d
    assert 'object_dataset_delete_user_week' in d
    assert 'object_image_insert_user_hour' in d
    assert 'object_screen_delete_day' in d
    assert 'object_plate_insert_user_week' in d
    assert 'object_well_delete_hour' in d


def test_search_target(client):
    target = {'target': 'fileset_insert_u'}
    rv = with_auth(
        client, 'GET', '/omero.example:4064/search', json=target)
    assert rv.status_code == 200
    d = rv.get_json()
    assert d == [
        'object_fileset_insert_user_day',
        'object_fileset_insert_user_hour',
        'object_fileset_insert_user_week',
    ]


def test_search_regex(client):
    target = {'target': 'project.*_[^u][^_]+_hour'}
    rv = with_auth(
        client, 'GET', '/omero.example:4064/search', json=target)
    assert rv.status_code == 200
    d = rv.get_json()
    assert d == [
        'object_project_delete_hour',
        'object_project_insert_hour',
    ]


def get_query_data(targettype):
    query_service_return = [
        [rint(1), rint(1522566000), rstring('alice')],
        [rint(2), rint(1522569600), rstring('bob')],
        [rint(3), rint(1522573200), rstring('alice')],
    ]

    grafana_query_data = {
        'range': {
            'from': '2018-04-01T06:00:00Z',  # epoch: 1522562400 seconds
            'to': '2018-04-01T10:00:00Z',    # epoch: 1522576800 seconds
        },
        'targets': [
            {
                'target': 'object_fileset_insert_hour',
                'type': targettype,
            },
        ],
    }
    return query_service_return, grafana_query_data


@patch('app.connect_omero')
def test_query_timeserie(mock_connect_omero, client):
    m = Mock(autospec=BlitzGateway)
    mock_connect_omero.return_value = m
    query_service_return, grafana_query_data = get_query_data('timeserie')
    m.getQueryService.return_value.projection.return_value = \
        query_service_return
    expected = [
        {
            'target': 'object_fileset_insert_hour{alice}',
            'datapoints': [[1, 1522566000000], [3, 1522573200000]],
        },
        {
            'target': 'object_fileset_insert_hour{bob}',
            'datapoints': [[2, 1522569600000]],
        }
    ]

    rv = with_auth(
        client, 'GET', '/omero.example:4064/query', json=grafana_query_data)
    assert rv.status_code == 200
    assert rv.get_json() == expected


@patch('app.connect_omero')
def test_query_table(mock_connect_omero, client):
    m = Mock(autospec=BlitzGateway)
    mock_connect_omero.return_value = m
    query_service_return, grafana_query_data = get_query_data('table')
    m.getQueryService.return_value.projection.return_value = \
        query_service_return
    expected = [
        {
            'columns': [
                {'text': 'count', 'type': 'time'},
                {'text': 'time', 'type': 'number'},
                {'text': 'user', 'type': 'string'},
            ],
            'rows': [
                [1, 1522566000000, 'alice'],
                [2, 1522569600000, 'bob'],
                [3, 1522573200000, 'alice'],
            ],
            'type': 'table',
        }
    ]

    rv = with_auth(
        client, 'GET', '/omero.example:4064/query', json=grafana_query_data)
    assert rv.status_code == 200
    assert rv.get_json() == expected
