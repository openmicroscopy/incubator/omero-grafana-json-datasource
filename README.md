# OMERO Grafana JSON Datasource

An experimental OMERO backend for the [Grafana JSON Datasource plugin](https://github.com/grafana/simple-json-datasource).


## Example (for development/testing only)

Build and run the Grafana Docker container:
```
docker build -t grafana-test grafana-docker
docker run -d -p 3000:3000 grafana-test
```
Run the backend application (listens on port 5000):
```
pip install requirements.txt
FLASK_DEBUG=1 flask run --host=0.0.0.0
```
- Open http://localhost:3000 and login as an admin
- Create a new SimpleJson datasource:
  - URL: http://your-external-ip:5000/ADDRESS.OF.OMERO.SERVER:PORT
  - Basic Auth: Ticked
  - User: OMERO username
  - Password: OMERO Password
  - Click Save & Test

Add a dashboard, for example [`grafana-docker/omero-session-lengths.json`](grafana-docker/omero-session-lengths.json).
