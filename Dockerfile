FROM registry.gitlab.com/openmicroscopy/incubator/omero-py-alpine/omero-py:5.4.9-1
MAINTAINER ome-devel@lists.openmicroscopy.org.uk

USER root

WORKDIR /opt/omero-grafana-json-datasource/
ADD app.py requirements.txt /opt/omero-grafana-json-datasource/
RUN pip install -r /opt/omero-grafana-json-datasource/requirements.txt && \
    echo /opt/omero/OMERO.py/lib/python > /usr/lib/python2.7/site-packages/omero.pth && \
    adduser -D -h /opt/omero-grafana-json-datasource/ grafana && \
    apk --no-cache add --virtual gevent-build-deps \
        build-base \
        python2-dev && \
    pip install gunicorn==19.9.0 gevent==1.3.7 && \
    apk del gevent-build-deps && \
    python -m compileall /opt/omero/OMERO.py/ /opt/omero-grafana-json-datasource/
# TODO: Should compileall in parent image instead
USER grafana

EXPOSE 5000
ENTRYPOINT ["gunicorn", "-b0.0.0.0:5000", "app:app", "--access-logfile=-", "--worker-class=gevent"]
