#!/usr/bin/env python
# OMERO backend for grafana json datasource
# https://github.com/grafana/simple-json-datasource
# Based on
# - https://gist.github.com/linar-jether/95ff412f9d19fdf5e51293eb0c09b850
# - http://flask.pocoo.org/snippets/8/

from flask import (
    abort,
    Flask,
    jsonify,
    request,
    Response,
)
from dateutil.parser import isoparse
from dateutil import tz
from datetime import datetime
from functools import wraps
import re

import omero.clients  # noqa
from omero.gateway import BlitzGateway
from omero.rtypes import (
    rint,
    unwrap,
)
from omero.sys import ParametersI


app = Flask(__name__)

methods = ('GET', 'POST')
max_data_points = 100
description = 'OMERO Grafana JSON Datasource'


def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
        'Login required %s' % description, 401,
        {'WWW-Authenticate': 'Basic realm="%s"' % description})


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not request.authorization:
            return authenticate()
        conn = connect_omero(request, kwargs['server'])
        if not conn:
            return authenticate()
        kwargs['conn'] = conn
        try:
            return f(*args, **kwargs)
        finally:
            conn.close()
    return decorated


session_length_template = """
    SELECT
        EXTRACT(EPOCH FROM closed) - EXTRACT(EPOCH FROM started),
        EXTRACT(EPOCH FROM {reference})
        {select_user}
    FROM Session
    WHERE
        EXTRACT(EPOCH FROM {reference}) >= :timestamp_start AND
        EXTRACT(EPOCH FROM {reference}) < :timestamp_end AND
        owner.omeName <> 'root'
    ORDER BY
        {reference} ASC
    """

eventlog_template = """
    SELECT
        {negate_count}COUNT(*),
        FLOOR(EXTRACT(EPOCH FROM event.time) / :interval) * :interval
        {select_user}
    FROM EventLog
    WHERE
        entityType='{entityType}' AND
        action='{action}' AND
        EXTRACT(EPOCH FROM event.time) >= :timestamp_start AND
        EXTRACT(EPOCH FROM event.time) < :timestamp_end
    GROUP BY
        FLOOR(EXTRACT(EPOCH FROM event.time) / :interval) * :interval
        {select_user}
    ORDER BY
        FLOOR(EXTRACT(EPOCH FROM event.time) / :interval) * :interval
    """

# First column: metric value as float
# Second column: unix timestamp in seconds (need to convert to
#   milliseconds later for grafana)
# Other columns: labels
# The following HQL parameters can be included in queries:
# - timestamp_start (int)
# - timestamp_end (int)
# - interval (int, currently doesn't work)
omero_queries = {
    # Query sessions which started in the queried interval
    'session_length': {
        'query': session_length_template.format(
            reference='started', select_user=''),
        'columns': ['length', 'started'],
    },
    'session_length_user': {
        'query': session_length_template.format(
            reference='started', select_user=', owner.omeName'),
        'columns': ['length', 'started', 'user'],
    },
}
interval_to_seconds = {
    'hour': 3600,
    'day': 86400,
    'week': 86400 * 7,
}
for action in ('INSERT', 'DELETE'):
    for entityType in ('ome.model.fs.Fileset',
                       'ome.model.containers.Project',
                       'ome.model.containers.Dataset',
                       'ome.model.core.Image',
                       'ome.model.screen.Screen',
                       'ome.model.screen.Plate',
                       'ome.model.screen.Well',
                       ):
        for interval in ('hour', 'day', 'week'):
            for user in ('', '_user'):
                name = 'object_%s_%s%s_%s' % (
                    entityType.split('.')[-1].lower(),
                    action.lower(),
                    user,
                    interval,
                )
                negate_count = '' if action == 'INSERT' else '-'
                select_user = ', event.experimenter.omeName' if user else ''
                omero_queries[name] = {
                    'query': eventlog_template.replace(
                        ':interval', str(interval_to_seconds[interval])
                        ).format(
                        negate_count=negate_count,
                        select_user=select_user,
                        entityType=entityType,
                        action=action,
                    ),
                    'columns': ['count', 'time', 'user'],
                }


@app.route('/<server>', methods=methods)
@requires_auth
def test_connection(server, conn):
    return '%s (%s)' % (description, server)


@app.route('/<server>/search', methods=methods)
def search(server):
    # regex search of metric names
    try:
        target = request.get_json().get('target', '')
    except AttributeError:
        target = ''
    r = sorted(k for k in omero_queries.keys() if re.search(target, k))
    return jsonify(r)


@app.route('/<server>/query', methods=methods)
@requires_auth
def query(server, conn):
    # hdr = request.headers
    req = request.get_json()
    # import pprint
    # pprint.pprint(hdr)
    # pprint.pprint(req)
    rs = query_request(conn, req)
    return rs


@app.route('/<server>/annotations')
@app.route('/<server>/tag-keys')
@app.route('/<server>/tag-values')
def empty(server):
    return jsonify([])


@app.route('/')
def home():
    return jsonify([])


def _utc_to_seconds(tstr):
    # https://stackoverflow.com/a/11743262/8062212
    ts = isoparse(tstr)
    epoch = datetime(1970, 1, 1, tzinfo=tz.UTC)
    return int((ts - epoch).total_seconds())


def _process_timeseries_results(metric, rs):
    if len(rs) < 1:
        return []
    target_rs = {}
    has_labels = (len(rs[0]) > 2)
    for r in rs:
        # Grafana requires timestamps in ms
        r[1] *= 1000
        if has_labels:
            t = '%s{%s}' % (metric, ','.join(str(e) for e in r[2:]))
        else:
            t = metric
        try:
            target_rs[t].append(r[:2])
        except KeyError:
            target_rs[t] = [r[:2]]

    resps = [{"target": tg, "datapoints": dp} for tg, dp in target_rs.items()]
    return resps


def _process_table_results(metric, types, rs):
    if len(rs) < 1:
        return []
    columns = [{"text": a, "type": b} for a, b in
               zip(omero_queries[metric]['columns'], types)]
    print columns
    for i in xrange(len(rs)):
        # Grafana requires timestamps in ms
        rs[i][1] *= 1000
    resps = [{"columns": columns, "rows": rs, "type": "table"}]
    return resps


def query_request(conn, j):
    ts_from = _utc_to_seconds(j['range']['from'])
    ts_to = _utc_to_seconds(j['range']['to'])
    interval = j.get('intervalMs', 0) / 1000
    max_data = min(j.get('maxDataPoints'), max_data_points)
    response = []
    for target in j['targets']:
        targettype = target.get('type', 'timeserie')
        targetmetric = target.get('target')
        if not targetmetric:
            # Grafana may send an empty request
            continue
        # print((interval, ts_from, ts_to, targettype, targetmetric))
        rs, types = query_omero(
            conn, omero_queries[targetmetric]['query'],
            ts_from, ts_to, interval, max_data)
        if targettype == 'timeserie':
            response.extend(_process_timeseries_results(targetmetric, rs))
        elif targettype == 'table':
            response.extend(_process_table_results(targetmetric, types, rs))
        else:
            abort(500, Exception('Invalid metric type: %s' % targettype))
    return jsonify(response)


def connect_omero(request, server):
    assert request.authorization
    user = request.authorization.username
    password = request.authorization.password
    try:
        host, port = server.split(':', 1)
    except ValueError:
        host = server
        port = 4064
    conn = BlitzGateway(user, password, host=host, port=port, secure=True)
    if not conn.connect():
        abort(404, Exception(
            'Connection to %s@%s:%s failed' % (user, host, port)))
    return conn


def _omero_to_grafana_type(rtype):
    if isinstance(rtype, (omero.RFloat, omero.RInt, omero.RLong)):
        return 'number'
    return 'string'


def query_omero(conn, q, ts_from, ts_to, interval, max_data):
    # TODO: may need to break query into batches
    # interval = int(max(interval, (ts_to - ts_from) / max_data))
    params = ParametersI()
    params.map['timestamp_start'] = rint(ts_from)
    params.map['timestamp_end'] = rint(ts_to)
    params.map['interval'] = rint(interval)
    # print q, params
    r = conn.getQueryService().projection(q, params)
    types = ['time']
    if len(r) > 0:
        types += [_omero_to_grafana_type(i) for i in r[0][1:]]
    return unwrap(r), types
